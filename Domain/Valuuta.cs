using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Valuuta
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string kood { get; set; }
        public string nimetus { get; set; }
    }
}