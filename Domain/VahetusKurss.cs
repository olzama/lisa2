using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class VahetusKurss
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public double Kurss { get; set; }
        public Valuuta From { get; set; }
        public int FromId { get; set; }
        public Valuuta To { get; set; }
        public int ToId { get; set; }
    }
}