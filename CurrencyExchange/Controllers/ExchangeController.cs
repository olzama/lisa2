using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExchangeController : ControllerBase
    {
        private IBLL _bll;
        public ExchangeController(IBLL bll)
        {
            _bll = bll;
        }
        [HttpPost]
        public double Post([FromBody] VahetusKurss rate)
        {
            var currencyFrom = _bll.Currencies.Get(rate.ValuutaFromCode);
            var currencyTo = _bll.Currencies.Get(rate.ValuutaToCode);
            return _bll.Exchanges.Add(currencyFrom, currencyTo, double.Parse(rate.Kurss, CultureInfo.InvariantCulture.NumberFormat));
        }
        
        [HttpGet]
        [Route("Rate")]
        public IEnumerable<KurssList> GetRate(string code)
        {
            return _bll.Exchanges.GetByFromCode(code);
        }
        
        [HttpGet]
        public double Get(string cur1, string cur2, string sum)
        {
            return _bll.Exchanges.Convert(cur1, cur2, Convert.ToInt32(sum));
        }
    }
}