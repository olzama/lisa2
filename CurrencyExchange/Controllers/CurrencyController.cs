using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CurrencyController : ControllerBase
    {
        private IBLL _bll;
        public CurrencyController(IBLL bll)
        {
            _bll = bll;
        }
        [HttpPost]
        public string Post([FromBody] Valuuta currency )
        {
            return _bll.Currencies.Add(currency);
        }
        
        [HttpGet]
        public async Task<ActionResult<List<string>>> Get()
        {
            return _bll.Currencies.GetAll();
        }
    }
}