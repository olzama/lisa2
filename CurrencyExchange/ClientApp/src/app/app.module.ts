import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import {UpdaterComponent} from './update/update.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import {ManageSummComponent} from "./manage-summ/manage-summ.component";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    UpdaterComponent,
    FetchDataComponent,
    ManageSummComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'updater', component: UpdaterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'manage-summ', component: ManageSummComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
