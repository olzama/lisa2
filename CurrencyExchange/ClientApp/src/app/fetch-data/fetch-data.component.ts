import { Component, Inject } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public rates: Rate[];
  public code;
  public baseUrl;

  public updateCode(code) {
    this.code = code;

  }
  public currencies: string[];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
    http.get<string[]>(baseUrl + 'currency').subscribe(result => {
      this.currencies = result;
    }, error => console.error(error));
  }

  public GetRates()
  {
    const params = new HttpParams({
      fromObject: {
        code: this.code,
      }
    });
    this.http.get<Rate[]>(this.baseUrl + 'exchange/rate', { params: params }).subscribe(result => {
      this.rates = result;
    }, error => console.error(error));
  }
}

interface Rate {
  codeTo: string;
  kurss: string;
}
