import {Component, Inject} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";

@Component({
  selector: 'manage-summ-component',
  templateUrl: './manage-summ.component.html'
})
export class ManageSummComponent {

  public currencies: string[];

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
    http.get<string[]>(baseUrl + 'currency').subscribe(result => {
      this.currencies = result;
    }, error => console.error(error));
  }

  public currentSumm = 0;
  public selectedCurrency1;
  public selectedCurrency2;
  public converted;
  public baseUrl;

  public incrementCounter() {
    this.currentSumm++;
  }

  public convert() {
    const params = new HttpParams({
      fromObject: {
        cur1: this.selectedCurrency1,
        cur2: this.selectedCurrency2,
        sum: this.currentSumm.toString()
      }
    });
    this.http.get<number>(this.baseUrl + 'exchange', { params: params }).subscribe(result => {
      this.converted = result.toString();
    }, error => console.error(error));
  }

  public decrementCounter() {
    this.currentSumm--;
  }
  public updateSum(event) {
    this.currentSumm = event.target.value;
  }
}
