import {Component, Inject} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";


@Component({
  selector: 'app-updater-component',
  templateUrl: './update.component.html'
})
export class UpdaterComponent {

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
    http.get<string[]>(baseUrl + 'currency').subscribe(result => {
      this.currencies = result;
    }, error => console.error(error));
  }
  public currencyCode;
  public currencyName;
  public updatedCode;
  public baseUrl;
  public currentRate;
  public updatedRate;
  public selectedCurrency1;
  public selectedCurrency2;
  public currencies: string[];

  public updateName(event: any) {
    this.currencyName = event.target.value;
  }

  public updateRate(event: any) {
    this.currentRate = event.target.value;
  }
  public updateCode(event: any) {
    this.currencyCode = event.target.value;
  }


  public addRate() {
    const rate: Rate = {
      Kurss: this.currentRate,
      ValuutaFromCode: this.selectedCurrency1,
      ValuutaToCode: this.selectedCurrency2
    };
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(rate);

    this.http.post<number>(this.baseUrl + 'exchange', body, {'headers':headers}).subscribe(data => {
      this.updatedRate = data;
    }, error => console.error(error));
  };

  public addCurrency() {
    const currency: Currency = {
      nimetus: this.currencyName,
      kood: this.currencyCode,
    };
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(currency);

    this.http.post<string>(this.baseUrl + 'currency', body, {'headers':headers}).subscribe(result => {
      this.updatedCode = result;
    }, error => console.error(error));
  };
}

interface Currency {
  kood: string;
  nimetus: string;
}

interface Rate {
  Kurss: string;
  ValuutaFromCode: string;
  ValuutaToCode: string
}
