using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using DAL;
using BusinessLayer;
using Domain;

namespace UnitTests
{
    public class TestConfig
    {

        public static DbContextOptions<ApplicationDbContext> _options = CreateOptions();
        public static ApplicationDbContext _context = new ApplicationDbContext(_options);
        public ExchangeService exchangeService = new ExchangeService(_context);
        public CurrencyService currencyService = new CurrencyService(_context);
        
        public static DbContextOptions<ApplicationDbContext> CreateOptions()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder
                { DataSource = ":memory:" };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);
            connection.Open();
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseSqlite(connection);
            return builder.Options;
        }

        public void seedCurrency()
        {
            var val1 = new Valuuta() {kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {kood = "USD", nimetus = "US Dollar"};
            currencyService.Add(val1);
            currencyService.Add(val2);
        }
        public void DatabaseSetup()
        {
            _context.Database.EnsureCreated();
        }
    }
}