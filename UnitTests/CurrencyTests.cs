using Domain;
using KellermanSoftware.CompareNetObjects;
using NUnit.Framework;

namespace UnitTests
{
    public class CurrencyTests
    {
        private static TestConfig _config;
        [SetUp]
        public void Setup()
        {
            _config = new TestConfig();
        }
        
        [Test]
        public void AddCurrencyTest()
        {
            _config.DatabaseSetup();
            CompareLogic compareLogic = new CompareLogic();
            compareLogic.Config.IgnoreProperty<Valuuta>(v => v.Id);
            var val1 = new Valuuta() {kood = "EUR", nimetus = "Euro"};
            _config.currencyService.Add(val1);
            var created = _config.currencyService.Get(val1.kood);
            Assert.IsNotNull(created);
            Assert.IsTrue(compareLogic.Compare(val1, created).AreEqual);
        }
        [Test]
        public void GetAllTest()
        {
            _config.DatabaseSetup();
            _config.seedCurrency();
            Assert.AreEqual(2, _config.currencyService.GetAll().Count);
        }
       
    }
}