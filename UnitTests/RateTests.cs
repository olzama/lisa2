using System;
using Domain;
using KellermanSoftware.CompareNetObjects;
using NUnit.Framework;

namespace UnitTests
{
    public class BookVisitTests
    {
        private static TestConfig _config;
        [SetUp]
        public void Setup()
        {
            _config = new TestConfig();
        }
        
        [Test]
        public void AddRateTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};

            _config.exchangeService.Add(val1, val2, 1.16f);
            var created = _config.exchangeService.Get("EUR", "USD");
            Assert.IsNotNull(created);
            Assert.AreEqual(created, created.Kurss);
        }
        
        [Test]
        public void ConvertTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};

            var result = _config.exchangeService.Add(val1, val2, 1.16f);
            var sum = _config.exchangeService.Convert("EUR", "USD", 2);
            Assert.IsNotNull(sum);
            Assert.AreEqual(System.Math.Round(2 * result, 4,  MidpointRounding.AwayFromZero), System.Math.Round(sum, 4, MidpointRounding.AwayFromZero));
        }
        
        [Test]
        public void GetByFromCodeRateTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};
            var val3 = new Valuuta() {Id = 3, kood = "GBP", nimetus = "Pound sterling"};
            _config.exchangeService.Add(val1, val2, 1.16f);
            _config.exchangeService.Add(val1, val3, 0.85f);
            var list = _config.exchangeService.GetByFromCode("EUR");
            Assert.IsNotNull(list);
            Assert.AreEqual(2, list.Count);
        }
        
        [Test]
        public void UpdateRateTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};
            var result = _config.exchangeService.Add(val1, val2, 1.16f);
            _config.exchangeService.Add(val1, val2, 1.17f);
            var updated = _config.exchangeService.Get("EUR", "USD");
            Assert.IsNotNull(updated);
            Assert.AreEqual(updated.Kurss, 1.17f);
        }
        
        [Test]
        public void AddReversedRateTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};
            _config.exchangeService.Add(val1, val2, 1.16f);
            var reversed = _config.exchangeService.Get("USD", "EUR");
            Assert.IsNotNull(reversed);
            Assert.AreEqual(System.Math.Round(1/1.16f, 4), System.Math.Round(reversed.Kurss, 4));
        }
        
        [Test]
        public void UpdateReversedRateTest()
        {
            _config.DatabaseSetup();
            var val1 = new Valuuta() {Id = 1, kood = "EUR", nimetus = "Euro"};
            var val2 = new Valuuta() {Id = 2, kood = "USD", nimetus = "US Dollar"};
            var result = _config.exchangeService.Add(val1, val2, 1.16f);
            _config.exchangeService.Add(val1, val2, 1.17f);
            _config.exchangeService.Get("EUR", "USD");
            var reversed = _config.exchangeService.Get("USD", "EUR");
            Assert.IsNotNull(reversed);
            Assert.AreEqual(System.Math.Round(1/1.17f, 4), System.Math.Round(reversed.Kurss, 4));
        }
    }
}