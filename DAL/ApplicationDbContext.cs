﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;
namespace DAL
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Valuuta> Currencies { get; set; }  = default!;
        public DbSet<VahetusKurss> Rates { get; set; }  = default!;
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
/*
            modelBuilder.Entity<Valuuta>().HasData(
                new Valuuta() { Id = 1, kood = "EUR", nimetus = "Euro"},
                new Valuuta() { Id = 2, kood = "USD", nimetus = "US Dollar"},
                new Valuuta() { Id = 3, kood = "GBP", nimetus = "Pound sterling"});
            modelBuilder.Entity<VahetusKurss>().HasData(
                new VahetusKurss() { Id = 1, FromId = 1, ToId = 2, Kurss = 1.16f},
                new VahetusKurss() { Id = 2, FromId = 2, ToId = 1, Kurss = 0.87f},
                new VahetusKurss() { Id = 3, FromId = 1, ToId = 3, Kurss = 0.85f},
                new VahetusKurss() { Id = 4, FromId = 3, ToId = 1, Kurss = 0.18f},
                new VahetusKurss() { Id = 6, FromId = 2, ToId = 3, Kurss = 0.74f},
                new VahetusKurss() { Id = 7, FromId = 3, ToId = 2, Kurss = 1.36f}
                
                );*/

            modelBuilder.Entity<Valuuta>()
                .Property(p => p.Id)
                .HasDefaultValueSql("NewId()");
            modelBuilder.Entity<VahetusKurss>()
                .Property(p => p.Id)
                .HasDefaultValueSql("NewId()");

        }

    }
}