using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using Domain;
using DTO;
using Microsoft.EntityFrameworkCore;
using VahetusKurss = Domain.VahetusKurss;

namespace BusinessLayer
{
    public class ExchangeService : IExchangeService
    {
        private DbContext _context;

        public ExchangeService(DbContext context)
        {
            _context = context;
        }

        public double Update(VahetusKurss currentRate, double rateValue)
        {
            currentRate.Kurss = rateValue;
            var res = _context.Set<VahetusKurss>().Update(currentRate);
            _context.SaveChanges();
            return res.Entity.Kurss;
        }

        public double Add(Valuuta from, Valuuta to, double rateValue)
        {
            var existingRate = Get(from.kood, to.kood);
            var existingReversed = Get(to.kood, from.kood);
            if (existingReversed == null)
            {
                AddRate(to, from, 1 / rateValue);
            }
            else
            {
                Update(existingReversed, 1 /rateValue);
            }
            return existingRate != null ? Update(existingRate, rateValue) : AddRate(from, to, rateValue);
        }

        public double AddRate(Valuuta from, Valuuta to, double rateValue)
        {
            var rate = new VahetusKurss()
            {
                From = from,
                FromId = from.Id,
                To = to,
                ToId = to.Id,
                Kurss = rateValue
            };
            var res = _context.Set<VahetusKurss>().Add(rate);
            _context.SaveChanges();
            return res.Entity.Kurss;
        }

        public VahetusKurss Get(string cur1, string cur2)
        {
            return _context
                .Set<VahetusKurss>()
                .Include(v => v.From)
                .Include(v => v.To)
                .FirstOrDefault(v => v.From.kood == cur1 && v.To.kood == cur2);
        }

        public List<KurssList> GetByFromCode(string codeFrom)
        {
            return _context
                .Set<VahetusKurss>()
                .Include(v => v.From)
                .Include(v => v.To)
                .Where(v => v.From.kood == codeFrom)
                .Select(rate =>
                    new KurssList {Kurss = rate.Kurss.ToString(CultureInfo.InvariantCulture), CodeTo = rate.To.kood}).ToList();
        }

        public double Convert(string cur1, string cur2, int sum)
        {
            var rate = Get(cur1, cur2);
            return sum * rate.Kurss;
        }
    }
    public interface IExchangeService
    {
        double Add(Valuuta from, Valuuta to, double rateValue);
        double Convert(string cur1, string cur2, int sum);
        List<KurssList> GetByFromCode(string codeFrom);
    }
}