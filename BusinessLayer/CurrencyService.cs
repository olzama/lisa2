using System.Collections.Generic;
using System.Linq;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer
{
    public class CurrencyService : ICurrencyService
    {
        private DbContext _context;
        public CurrencyService(DbContext context)
        {
            _context = context;
        }

        public string Add(Valuuta currency)
        {
            var res = _context.Set<Valuuta>().Add(currency);
            _context.SaveChanges();
            return res.Entity.kood;
        }
        
        public Valuuta Get(string code)
        {
            return _context
                .Set<Valuuta>()
                .FirstOrDefault(v => v.kood == code );
        }
        
        public List<string> GetAll()
        {
            return _context.Set<Valuuta>().Select(v => v.kood).ToList();
        }
    }
    
    public interface ICurrencyService
    {
        string Add(Valuuta currency);
        List<string> GetAll();
        Valuuta Get(string code);
    }
}