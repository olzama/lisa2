using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer
{
    public class BLL : IBLL
    {
        private DbContext _context;

        public BLL(DbContext context)
        {
            _context = context;
        }
        public IExchangeService Exchanges =>
            GetService<IExchangeService>(() => new ExchangeService(_context));

        public ICurrencyService Currencies =>
            GetService<ICurrencyService>(() => new CurrencyService(_context));

        private readonly Dictionary<Type, object> _repoCache = new Dictionary<Type, object>();

        // Factory method
        public TService GetService<TService>(Func<TService> serviceCreationMethod)
        {
            if (_repoCache.TryGetValue(typeof(TService), out var repo))
            {
                return (TService)repo;
            }

            repo = serviceCreationMethod()!;
            _repoCache.Add(typeof(TService), repo);
            return (TService)repo;
        }
    }
    
    
}