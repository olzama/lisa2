﻿using System;

namespace BusinessLayer
{
    public interface IBLL
    {
        IExchangeService Exchanges { get; }
        ICurrencyService Currencies { get; }
    }
}