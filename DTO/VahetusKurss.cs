using System.ComponentModel.DataAnnotations.Schema;

namespace DTO
{
    public class VahetusKurss
    {
        public string Kurss { get; set; }
        public string ValuutaFromCode { get; set; }
        public string ValuutaToCode { get; set; }
    }
}